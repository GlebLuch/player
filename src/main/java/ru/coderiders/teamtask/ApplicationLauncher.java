package ru.coderiders.teamtask;

import ru.coderiders.teamtask.core.PlayerMainWindowApplication;

public class ApplicationLauncher {
    public static void main(String[] args) {
        PlayerMainWindowApplication.main(args);
    }
}
