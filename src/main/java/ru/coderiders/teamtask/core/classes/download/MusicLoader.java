package ru.coderiders.teamtask.core.classes.download;

import ru.coderiders.teamtask.core.classes.AudioFile;

import java.io.IOException;
import java.net.URISyntaxException;

public class MusicLoader {
    public static AudioFile saveMp3(String from, String to) throws InterruptedException, IOException, URISyntaxException {
        var thread = new MediaThread(from, to);
        String tempTitle = from.substring(from.lastIndexOf("/") + 1);
        thread.setName(tempTitle);
        thread.start();
        thread.join();
        System.out.println("Загрузили " + to + tempTitle);
        AudioFile file = new AudioFile(to + tempTitle);
        file.renameTo(new AudioFile(to + TagProvider.getTitle(to + tempTitle)));
        System.out.println("Сохранили " + file);
        return file;
    }
}
