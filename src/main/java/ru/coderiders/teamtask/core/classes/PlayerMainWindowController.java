package ru.coderiders.teamtask.core.classes;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ru.coderiders.teamtask.core.PlayerMainWindowApplication;
import ru.coderiders.teamtask.core.classes.download.TagProvider;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class PlayerMainWindowController implements Initializable {
    MediaPlayer player;
    AudioFile current;
    public List<AudioFile> playList = new LinkedList<>();

    @FXML
    private Button pauseButton;

    @FXML
    private Slider progressSlider;

    @FXML
    private ImageView imageView;

    @FXML
    private Label songLabel;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {}

    @FXML
    protected void onPauseButtonClick() {
        try {
            var status = player.getStatus();
            if (status == MediaPlayer.Status.PLAYING) {
                player.pause();
            } else {
                player.play();
            }
        } catch (Exception ignored) {
            playAnotherAudio(current);
        }
    }

    @FXML
    protected void onBackButtonClick() {
        if (getPrevious(current) != null) {
            this.onPauseButtonClick();
            playAnotherAudio(getPrevious(current));
            current = getPrevious(current);
        }
    }

    @FXML
    protected void onNextButtonClick() {
        if (getNext(current) != null) {
            this.onPauseButtonClick();
            playAnotherAudio(getNext(current));
            current = getNext(current);
        }
    }

    @FXML
    public void shutdown() {
        var stage = (Stage) pauseButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void openFile() throws IOException, URISyntaxException {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Выберете аудио-файл");
        var path = chooser.showOpenDialog(pauseButton.getScene().getWindow())
                .toPath().toString();
        AudioFile file;
        try {
            file = new AudioFile(path, TagProvider.getImage(path));
        } catch (Exception ignored) {
            file = new AudioFile(path);
        }
        this.playList.add(file);
    }

    @FXML
    public void showPlayListEditor() throws IOException {
        var stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(PlayerMainWindowApplication.class.getResource("PlaylistWindow.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 640, 400);
        stage.setTitle("Плеер");
        stage.setScene(scene);
        var controller = (PlayerPlaylistWindowController) fxmlLoader.getController();
        controller.passPlayList(this.playList);
        controller.passController(this);
        stage.show();
    }

    @FXML
    public void shiftTime() {
        player.seek(player.getCycleDuration().multiply(progressSlider.getValue() / 100.0));
    }

    public AudioFile getNext(AudioFile file) {
        if (current != null) {
            int idx = playList.indexOf(file);
            if (idx < 0 || idx + 1 == playList.size()) return null;
            return playList.get(idx + 1);
        } else {
            this.current = playList.get(0);
            return this.current;
        }
    }

    public AudioFile getPrevious(AudioFile file) {
        if (current != null) {
            int idx = playList.indexOf(file);
            if (idx <= 0) return null;
            return playList.get(idx - 1);
        } else {
            this.current = playList.get(0);
            return this.current;
        }
    }

    private void playAnotherAudio(AudioFile file) {
        System.out.println("Playing " + file);
        var url = file.toURI().toString();
        if (this.player != null) {
            if (this.player.getStatus() == MediaPlayer.Status.PLAYING
                    || this.player.getStatus() == MediaPlayer.Status.PAUSED) {
                this.player.stop();
            }
        }
        this.player = new MediaPlayer(new Media(url));
        this.player.play();
        this.imageView.setImage(file.getCover());
        this.player.currentTimeProperty().addListener((observable, oldValue, newValue) ->
                progressSlider.setValue(player.getCycleDuration().divide(100).toSeconds()));
        this.songLabel.setText(file.getName());
        this.songLabel.setAlignment(Pos.CENTER);
    }
}