package ru.coderiders.teamtask.core.classes;

import javafx.scene.image.Image;
import ru.coderiders.teamtask.core.PlayerMainWindowApplication;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Objects;

public class AudioFile extends File {
    String name;
    Image cover;

    public AudioFile(String audioPath) throws URISyntaxException, MalformedURLException {
        super(audioPath);
        this.name = audioPath;
        cover = new Image(Objects.requireNonNull(PlayerMainWindowApplication.class
                .getResource("default.png")).toURI().toURL().toString());
    }

    public AudioFile(String audioPath, byte[] image){
        super(audioPath);
        this.name = audioPath;
        cover = new Image(new ByteArrayInputStream(image));
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String getName() {
        return this.name;
    }

    public Image getCover() {
        return cover;
    }
}
