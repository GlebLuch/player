package ru.coderiders.teamtask.core.classes;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import ru.coderiders.teamtask.core.classes.download.MusicLoader;
import ru.coderiders.teamtask.core.classes.download.TagProvider;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PlayerPlaylistWindowController implements Initializable {
    List<AudioFile> playList;
    PlayerMainWindowController controller;

    @FXML
    private ListView<AudioFile> listItem;

    @FXML
    private TextField savePathTextField;

    @FXML
    private TextField audioLinkTextField;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {}

    @FXML
    public void passController(PlayerMainWindowController cont) {
        this.controller = cont;
    }

    @FXML
    public void passPlayList(List<AudioFile> list) {
        this.playList = list;
        listItem.getItems().addAll(this.playList);
    }

    @FXML
    public void addItemToList() throws IOException, URISyntaxException, InterruptedException {
        var savePath = this.savePathTextField.getText();
        var audioUrl = this.audioLinkTextField.getText();
        String savedPath = MusicLoader.saveMp3(audioUrl, savePath).toPath().toString();


        if (!savePath.equals("") & !audioUrl.equals("")) {
            var image = TagProvider.getImage(savedPath);
            var file = new AudioFile(savedPath);
            if (image != null) {
                file = new AudioFile(savedPath, image);
            }
            listItem.getItems().add(file);
            this.controller.playList.add(file);
        }
    }
}
