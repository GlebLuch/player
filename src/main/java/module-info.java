module ru.coderiders.teamtask {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.jsoup;
    requires org.json;
    requires com.fasterxml.jackson.databind;
    requires javafx.media;
    requires jaudiotagger;


    exports ru.coderiders.teamtask.core;
    opens ru.coderiders.teamtask.core to javafx.fxml;
    exports ru.coderiders.teamtask.core.classes;
    opens ru.coderiders.teamtask.core.classes to javafx.fxml;
}